import { Injectable } from '@angular/core';
import { Users } from '../models/user.model';
import { HttpClient ,HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  // Define API
  apiURL = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  login(users : Users):Observable<Users>{
    return this.http.post<Users>(this.apiURL + '/users/login', JSON.stringify(users), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
}
