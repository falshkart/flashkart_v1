import { Component, OnInit } from '@angular/core';
import { Users } from '../models/user.model';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLogin:boolean=true;
  users: Users = new Users();
  constructor(public router: Router,private userService:UsersService) { }

  ngOnInit() {
  }

  toggleLoginTab():void{
    this.isLogin = !this.isLogin;
  }

  login(users : Users):void{
    this.userService.login(users).subscribe((data: {}) => {
      if(data){
        this.router.navigate(['/flashkart/home'])
      }else{
        alert("wrong credentials");
      }
      
    });
  }
}
