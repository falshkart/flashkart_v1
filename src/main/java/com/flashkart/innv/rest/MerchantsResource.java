package com.flashkart.innv.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flashkart.innv.model.Merchants;
import com.flashkart.innv.service.MerchantsService;

@CrossOrigin("*")
@RestController
@RequestMapping("/merchant")

public class MerchantsResource {
	
	@Autowired
	MerchantsService merchantService;
	
	@GetMapping("/allmerchants")
	public List<Merchants> getAllMerchant(){
		return merchantService.getAllMerchant();
	}
	
	@PostMapping("/savemerchant")
	public Merchants saveMerchant(Merchants Merchant) {
		return merchantService.saveMerchant(Merchant);
	}	
}


