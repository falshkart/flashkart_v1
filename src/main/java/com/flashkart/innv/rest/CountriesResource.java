package com.flashkart.innv.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flashkart.innv.model.Countries;
import com.flashkart.innv.service.CountriesService;

@CrossOrigin("*")
@RestController
@RequestMapping("/countries")

public class CountriesResource {

	@Autowired
	CountriesService countriesService;
	
	@GetMapping("/allcountries")
	public List<Countries> getAllCountries(){
		return countriesService.getAllCountries();
	}
	
}
