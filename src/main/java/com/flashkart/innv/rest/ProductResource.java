package com.flashkart.innv.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flashkart.innv.model.Product;
import com.flashkart.innv.service.ProductService;

@CrossOrigin("*")
@RestController
@RequestMapping("/product")
public class ProductResource {
	
	@Autowired
	ProductService productService;
	
	@GetMapping("/allproducts")
	public List<Product> getAllProduct(){
		return productService.getAllProduct();
	}
	
	@PostMapping("/saveproduct")
	public Product saveProduct(Product Product) {
		return productService.saveProduct(Product);
	}	
}



