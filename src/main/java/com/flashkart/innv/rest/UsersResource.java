package com.flashkart.innv.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flashkart.innv.model.Users;
import com.flashkart.innv.service.UsersService;

@CrossOrigin("*")
@RestController
@RequestMapping("/users")
public class UsersResource {

	@Autowired
	UsersService usersService;
	
	@GetMapping("/allusers")
	public List<Users> getAllUsers(){
		return usersService.getAllUsers();
	}
	
	@PostMapping("/saveuser")
	public Users saveUsers(Users Users) {
		return usersService.saveUsers(Users);
	}
	
	@PostMapping("/forgotpassword")
	public Users findByUsername(@RequestBody String username) {
		return usersService.forgotPassword(username);
	}
	
	@PostMapping("/login")
	public Users login(@RequestBody Users users) {
		return usersService.login(users.getEmail(),users.getPassword());
	}
	
}
