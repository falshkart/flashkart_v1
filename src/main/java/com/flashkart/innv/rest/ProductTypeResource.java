package com.flashkart.innv.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flashkart.innv.model.ProductType;
import com.flashkart.innv.service.ProductTypeService;

@CrossOrigin("*")
@RestController
@RequestMapping("/producttype")


public class ProductTypeResource {
	
	@Autowired
	ProductTypeService productTypeService;
	
	@GetMapping("/allproductType")
	public List<ProductType> getAllProductType(){
		return productTypeService.getAllProductType();
	}
	
	@PostMapping("/saveproductType")
	public ProductType saveProductType(ProductType ProductType) {
		return productTypeService.saveProductType(ProductType);
	}	


}
