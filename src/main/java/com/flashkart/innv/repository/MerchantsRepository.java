package com.flashkart.innv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flashkart.innv.model.Merchants;

@Repository
public interface MerchantsRepository extends JpaRepository<Merchants, Integer>{

}
