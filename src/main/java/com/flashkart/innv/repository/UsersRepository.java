package com.flashkart.innv.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flashkart.innv.model.Users;
@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {
	List<Users> findByEmail(String email);
	List<Users> findByEmailAndPassword(String email,String password);
}
