package com.flashkart.innv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flashkart.innv.model.ProductType;

@Repository

public interface ProductTypeRepository extends JpaRepository<ProductType, Integer>{

}
