package com.flashkart.innv.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="products_type")	
public class ProductType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@Column(name="products_cat")
	private String productsCat ;
	
	@Column(name="isactive ")
	private Integer isactive ;
	
	@JsonIgnore
	@OneToMany(mappedBy = "productType", cascade = CascadeType.ALL)
    private List<Product> products;


	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productsCat
	 */
	public String getProductsCat() {
		return productsCat;
	}

	/**
	 * @param productsCat the productsCat to set
	 */
	public void setProductsCat(String productsCat) {
		this.productsCat = productsCat;
	}

	/**
	 * @return the isactive
	 */
	public Integer getIsactive() {
		return isactive;
	}

	/**
	 * @param isactive the isactive to set
	 */
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	
}