package com.flashkart.innv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flashkart.innv.model.Countries;
import com.flashkart.innv.repository.CountriesRepository;

@Service
public class CountriesService {

	@Autowired
	CountriesRepository countriesRepository;
	
	public List<Countries> getAllCountries(){
		return countriesRepository.findAll();
	}
}
