package com.flashkart.innv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flashkart.innv.model.Merchants;
import com.flashkart.innv.repository.MerchantsRepository;

@Service

public class MerchantsService {
	
	@Autowired
	MerchantsRepository merchantRepository;
	
	public List<Merchants> getAllMerchant(){
		return merchantRepository.findAll();
	}
	
	public Merchants saveMerchant(Merchants Merchant) {
		return merchantRepository.save(Merchant);
	}
}	



