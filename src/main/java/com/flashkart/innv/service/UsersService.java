package com.flashkart.innv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flashkart.innv.model.Users;
import com.flashkart.innv.repository.UsersRepository;

@Service
public class UsersService {

	@Autowired
	UsersRepository usersRepository;
	
	public List<Users> getAllUsers(){
		return usersRepository.findAll();
	}
	
	public Users saveUsers(Users  Users) {
		return usersRepository.save(Users);
	}
	
	public Users forgotPassword(String username){
		if (usersRepository.findByEmail(username).isEmpty()) {
			return null;
		}else {
			return usersRepository.findByEmail(username).get(0);
		}
	}
	
	public Users login(String email, String password){
		List<Users> userData = (List<Users>) usersRepository.findByEmailAndPassword(email, password);
		if (userData.isEmpty()) {
			return null;
		}else {
			return (Users) usersRepository.findByEmailAndPassword(email, password).get(0);
		}
	}
}
