package com.flashkart.innv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flashkart.innv.model.ProductType;
import com.flashkart.innv.repository.ProductTypeRepository;

@Service

public class ProductTypeService {
	
	@Autowired
	ProductTypeRepository productsTypeRepository;
	
	public List<ProductType> getAllProductType(){
		return productsTypeRepository.findAll();
	}
	
	public ProductType saveProductType(ProductType ProductType) {
		return productsTypeRepository.save(ProductType);
	}

}
