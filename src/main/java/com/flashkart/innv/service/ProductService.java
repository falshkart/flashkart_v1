package com.flashkart.innv.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flashkart.innv.model.Product;
import com.flashkart.innv.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	public List<Product> getAllProduct(){
		return productRepository.findAll();
	}
	
	public Product saveProduct(Product Product) {
		return productRepository.save(Product);
	}
}	


