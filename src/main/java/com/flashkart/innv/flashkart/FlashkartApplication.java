package com.flashkart.innv.flashkart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.flashkart.innv.repository")
@EntityScan("com.flashkart.innv.model")
@ComponentScan({"com.flashkart.innv"})

public class FlashkartApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(FlashkartApplication.class, args);
	}

}
