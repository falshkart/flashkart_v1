CREATE TYPE "orders_status" AS ENUM (
  'created',
  'running',
  'done',
  'failure'
);

CREATE TABLE "orders" (
  "id" int PRIMARY KEY,
  "user_id" int UNIQUE NOT NULL,
  "status" "orders_status",
  "created_at" varchar
);

COMMENT ON COLUMN "orders"."created_at" IS 'When order created';

CREATE TABLE "order_items" (
  "order_id" int,
  "product_id" int,
  "quantity" int
);

CREATE TABLE "offers_cat" (
  "id" int PRIMARY KEY,
  "product_id" int,
  "user_id" int,
  "status" int,
  "created_at" varchar
);

CREATE TABLE "wish_list" (
  "id" int PRIMARY KEY,
  "product_id" int,
  "user_id" int,
  "status" int,
  "created_at" varchar
);

CREATE TABLE "products_type" (
  "id" int PRIMARY KEY,
  "products_cat" varchar,
  "isactive" int
);

CREATE TABLE "products" (
  "id" int PRIMARY KEY,
  "products_type_id" int,
  "name" varchar,
  "merchant_id" int NOT NULL,
  "price" int,
  "status" varchar,
  "created_at" varchar
);

CREATE TABLE "users" (
  "id" int PRIMARY KEY,
  "full_name" varchar,
  "email" varchar UNIQUE,
  "gender" varchar,
  "date_of_birth" varchar,
  "created_at" varchar,
  "country_code" int
);

CREATE TABLE "merchants" (
  "id" int PRIMARY KEY,
  "merchant_name" varchar,
  "country_code" int,
  "created_at" varchar,
  "admin_id" int
);

CREATE TABLE "countries" (
  "code" int PRIMARY KEY,
  "name" varchar,
  "continent_name" varchar
);

CREATE TABLE "cart" (
  "id" int PRIMARY KEY,
  "product_id" int,
  "user_id" int,
  "count" int,
  "status" int,
  "created_at" varchar
);

ALTER TABLE "order_items" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

ALTER TABLE "order_items" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "offers_cat" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "offers_cat" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "wish_list" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "wish_list" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("products_type_id") REFERENCES "products_type" ("id");

ALTER TABLE "users" ADD FOREIGN KEY ("country_code") REFERENCES "countries" ("code");

ALTER TABLE "merchants" ADD FOREIGN KEY ("country_code") REFERENCES "countries" ("code");

ALTER TABLE "products" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchants" ("id");

ALTER TABLE "merchants" ADD FOREIGN KEY ("admin_id") REFERENCES "users" ("id");

ALTER TABLE "cart" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

ALTER TABLE "cart" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");