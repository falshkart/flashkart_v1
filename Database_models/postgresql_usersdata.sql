CREATE TABLE users_data
(
  id integer NOT NULL DEFAULT nextval('users_data_seq'::regclass),
  username text,
  created_by text,
  created_date date,
  CONSTRAINT pk_id_users_data PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users_data
  OWNER TO postgres; 

  CREATE SEQUENCE users_data_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE users_data_seq
  OWNER TO postgres; 

CREATE SEQUENCE hibernate_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 299
  CACHE 1;
ALTER TABLE hibernate_sequence
  OWNER TO postgres; 